import { ActiveQueuesService } from './../services/active-queues.service';
import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';

import { PhoneNumber } from './../modules/phonenumber';
import { WindowService } from './../services/window.service';

@Component({
  selector: 'phone-login',
  templateUrl: './phone-login.component.html'
})
export class PhoneLoginComponent implements OnInit {
  windowRef: any;
  phoneNumber = new PhoneNumber()
  verificationCode: string;
  user: any;
  israelPhone: string;
  userName: string;
  activeQueue:string;
  errorMessege:string;
  key:string;

  constructor(private win: WindowService,
    private route: ActivatedRoute,
    private userService: UserService,
    private activeQueses:ActiveQueuesService) { }

  ngOnInit() {
    this.windowRef = this.win.windowRef
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')
    this.windowRef.recaptchaVerifier.render();

    let userName = this.route.snapshot.queryParamMap.get('user') || '/';
    let activeDate=this.route.snapshot.queryParamMap.get('date'); 
    let queueKey=this.route.snapshot.queryParamMap.get('queueKey');

    if (userName) this.userName = userName;
    if(activeDate)this.activeQueue=activeDate;  
    if(queueKey)this.key=queueKey;  
    
  }
  sendLoginCode(res) { 
    const phoneVal = res.slice(1);
     //console.log(phoneVal)
    this.israelPhone = phoneVal;
    const appVerifier = this.windowRef.recaptchaVerifier;
    const num = this.phoneNumber.e164;
    firebase.auth().signInWithPhoneNumber("+972"+ this.israelPhone, appVerifier)
      .then(result => {
        this.windowRef.confirmationResult = result; 
        this.userService.create(phoneVal);
      })
      .catch(error => console.log(error));
  }
  verifyLoginCode() {
    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then(result => {
        this.user = result.user; 
        this.activeQueses.create(this.activeQueue,this.userName)
        this.activeQueses.delete( this.key);
      })
      .catch(error => console.log(error, "Incorrect code entered?"));
  }
}