import { Pipe, PipeTransform } from '@angular/core';
import { Queue } from '../../modules/queue';

@Pipe({
    name: 'orderBy'
})

export class OrderByPipe implements PipeTransform {

    // queues: Queue[] = [];

    transform(array: Array<string>, filterField: string) {
        let sortedItems=[];
        switch (filterField) {
            case "date":
                sortedItems = array.sort((a: any, b: any) =>
                b.dateCreated - a.dateCreated
            );
                break;
            case "name":  
                   sortedItems = array.sort((one:any, two:any) => (one.barberName > two.barberName ? -1 : 1)); 
                break; 
            default:
                break;
        }
       
        return sortedItems;
    }
}
