import { Component } from '@angular/core';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth-service.service';
import { Router } from '@angular/router';
import { AppsettingsService } from './services/appsettings.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {  
  public langData: any = {}

  constructor(
    private appSettingsService : AppsettingsService) { }

ngOnInit(){
    this.appSettingsService.getJSON().subscribe(data => {
         this.langData=data;
      });
   } 
}
