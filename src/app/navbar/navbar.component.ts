import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { AuthService } from '../services/auth-service.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent {
  @Input() langData: any = {};

  constructor(private auth: AuthService) {

  }
  checkUserLoggedIn(){
    return localStorage.getItem('isLoggedIn') ? true : false;
  }
  onLogout(){
    this.auth.logout();
  }
  
}
