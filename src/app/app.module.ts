/*angular libs */
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import {Component, Inject} from '@angular/core'; 
import { JsonpModule } from '@angular/http';
import { ActiveQueuesService } from './services/active-queues.service';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated';  
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AngularFontAwesomeModule } from 'angular-font-awesome'; 
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import {MatTabsModule} from '@angular/material/tabs'; 
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSidenavModule} from '@angular/material/sidenav';
import {CountDown} from "../../node_modules/angular2-simple-countdown/countdown";
import { environment } from '../environments/environment';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import {SidebarModule } from 'ng-sidebar';
import {Ng2PageScrollModule} from 'ng2-page-scroll'; 
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

/*components */
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PhoneLoginComponent } from './phone-login/phone-login.component';
import { QueueComponent } from './queue/queue.component';
import { InstaGalleryComponent } from './insta-gallery/insta-gallery.component';
import { ActiveQueuesComponent } from './active-queues/active-queues.component';
/*pipes */
import { FilterQueuesPipe } from './common/pipes/filter-queues.pipe';
import { OrderByPipe  } from './common/pipes/OrderByPipe';
 
/*services*/
import { AuthService } from './services/auth-service.service';
import { QueueService } from './services/queue.service';
import { UserService } from './services/user.service';
import { WindowService } from './services/window.service';
import { AuthGuard } from './services/auth-guard.service';
import { FilterAlgoService } from './services/filter-algo.service';
import { AppsettingsService } from './services/appsettings.service'; 
 


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    QueueComponent,
    PhoneLoginComponent,
    ActiveQueuesComponent,
    InstaGalleryComponent,
    CountDown,
    FilterQueuesPipe,
    OrderByPipe  
  ],
  imports: [
    BrowserModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule,
    BrowserAnimationsModule, 
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, 
    AngularFireAuthModule, 
    AngularFontAwesomeModule, 
    MatFormFieldModule,  
    MatInputModule,
    JsonpModule,
    HttpClientModule,
    HttpClientJsonpModule, 
    Ng2CarouselamosModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSidenavModule, 
    Ng2PageScrollModule,
    SidebarModule.forRoot(),
    NgbModule.forRoot(),
    RouterModule.forRoot([
      { path: '', component: HomeComponent } ,
      { path: 'phone-login', component: PhoneLoginComponent},
      { path: 'insta-gallery', component: InstaGalleryComponent },
      { path: 'login', component: LoginComponent },
      { path: 'queue', component: QueueComponent ,canActivate:[AuthGuard]}   
    ])
  ],
  providers: [
    AppsettingsService,
    QueueService,
    UserService,
    AuthService, 
    WindowService,
    AuthGuard,
    FilterAlgoService,
    ActiveQueuesService, 
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'he'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
