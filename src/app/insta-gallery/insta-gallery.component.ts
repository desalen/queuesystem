import { HomeComponent } from '../home/home.component';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import {Jsonp} from '@angular/http'
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'insta-gallery',
  templateUrl: './insta-gallery.component.html',
  styleUrls: ['./insta-gallery.component.less']
})
export class InstaGalleryComponent implements OnInit {
  instaObj; 
  private url= 'https://api.instagram.com/v1/users/self/media/recent/?access_token=15217401699.1677ed0.8f9eda42d8554a06813280186a8c166d&callback=JSONP_CALLBACK';
  
  @Input ('instaInput') instaInput:HomeComponent;
  
  constructor(private _jsonp: Jsonp) { }
  ngOnInit() {
    // #access_token=15217401699.1677ed0.8f9eda42d8554a06813280186a8c166d
    this._jsonp.request(this.url, { method: 'Get' })
     .map(res => res.json())
     .subscribe((res) => { 
       this.instaObj=res.data;   
     });
  }  
}

