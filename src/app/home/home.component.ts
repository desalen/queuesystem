import { appClient } from './../modules/user';
import { Queue } from './../modules/queue';
import { QueueService } from './../services/queue.service';
import { Component, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { FilterAlgoService } from '../services/filter-algo.service';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
}) 
export class HomeComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  queuePanel = {};
  queues: Queue[] = [];
  btnValidty = true;
  highlightedRow;
  counter0 = 0;
  timer0Id: string;
  today;
  step2: boolean = false;
  baseQueues: Queue[] = [];
  userName:appClient;
  sortorder:string="date";

  constructor(private queueService: QueueService ) { 
    this.subscription = this.queueService.getAll()
      .subscribe((queues) => {
        this.queues = queues
        this.baseQueues = this.queues; 
      }); 
  }
  ngOnInit() {
    this.today = Date.now();
  }
  rowClick(val, $event) {
    // {barberName: ""Sam"", dateCreated: 1510752221136, queueTime: ""2017-11-22T01:03:00.000Z"", $key: "-KyzqNod2AXOOBW0Ftlw", $exists: ƒ}
    if (this.step2) this.step2 = false
    this.step2 = true;
    this.queuePanel = val;
    this.btnValidty = false;
    this.highlightedRow = val;
    var newArray = this.queues.filter((item: any) => {
      var a = new Date(item.queueTime).getDate();
      var b = new Date(val.queueTime).getDate();
      return a == b
    })
    this.queues = newArray;  
    this.isInvalid();
  }
  backBtn() {
    this.step2 = false;
    this.queues = this.baseQueues;
  }
  isInvalid() {
    let valid = this.btnValidty;
    return valid;
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
