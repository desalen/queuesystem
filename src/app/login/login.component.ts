import { admin } from './../modules/admin';
import { AuthService } from '../services/auth-service.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent {
  form = {};
 private errMessage;
  constructor(private auth:AuthService) { 
  }

  model = new admin(this.model, this.model);

  submitted = false;

  save(data) {
    this.submitted = true; 
    this.form=data;     
    this.auth.login(data); 
  }

  // login() {
  //   this.auth.login();
  // }

}
