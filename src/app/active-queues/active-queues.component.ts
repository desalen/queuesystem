import { activeQueues } from '../modules/activeQueues';
import { Subscription } from 'rxjs/Rx';
import { ActiveQueuesService } from '../services/active-queues.service';
import { HomeComponent } from './../home/home.component';
import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'active-queues',
  templateUrl: './active-queues.component.html',
  styleUrls: ['./active-queues.component.less']
})
export class ActiveQueuesComponent implements OnInit{

  currentQueues: activeQueues[] = [];
  subscription: Subscription;
  public highlightedRow;
  public isActive;  
  text: any = {
    "Weeks": "Weeks",
    "Days": "Days", "Hours": "Hours",
    "Minutes": "Minutes", "Seconds": "Seconds",
    "MilliSeconds": "MilliSeconds"
  };
  ngOnInit() {
  }
  //nowTime= Date.now() ;

  @Input('activeInput') activeInput: HomeComponent;

  constructor(private activeQueses: ActiveQueuesService) {
    this.subscription = this.activeQueses.getAll()
      .subscribe(currentQueues => this.currentQueues = currentQueues);
    //  .subscribe(active =>console.log(active));

  }
  rowClick(val) {
    const versionText = this.isActive ? "This is Version 1:" : "Switched to Version 2";
 
    // const currQueue = val.queueDate;
    // this.highlightedRow = val;
    this.activeQueses.update(val);
  }
  deleteAq(row){
    this.activeQueses.deleteActiveQueue(row.$key);
  }
}
