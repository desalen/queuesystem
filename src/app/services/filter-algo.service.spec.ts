import { TestBed, inject } from '@angular/core/testing';

import { FilterAlgoService } from './filter-algo.service';

describe('FilterAlgoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FilterAlgoService]
    });
  });

  it('should be created', inject([FilterAlgoService], (service: FilterAlgoService) => {
    expect(service).toBeTruthy();
  }));
});
