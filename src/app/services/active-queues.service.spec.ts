import { TestBed, inject } from '@angular/core/testing';

import { ActiveQueuesService } from './active-queues.service';

describe('ActiveQueuesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActiveQueuesService]
    });
  });

  it('should be created', inject([ActiveQueuesService], (service: ActiveQueuesService) => {
    expect(service).toBeTruthy();
  }));
});
