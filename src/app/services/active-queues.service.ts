import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database-deprecated'; 

@Injectable()
export class ActiveQueuesService {

  constructor(private db: AngularFireDatabase) { }

  getAll() {
    return this.db.list('/activeQueses/');
  }
  create(activeDate, clientName) {
    console.log(activeDate);
    return this.db.list('/activeQueses').push({
      queueDate: activeDate,
      client: clientName,
      isActive: false
    });

  }
  delete(key) {
    // console.log(key);
    return this.db.object('/queues/' + key)
      .remove()
      .then(x => console.log("the column as been deleted"));
  }
  deleteActiveQueue(key) {
    // console.log(key);
    return this.db.object('/activeQueses/' + key) 
      .remove()
      .then(x => console.log("the column as been deleted"));
  }
  update(obj) {
    let checkQueue = obj.isActive;
    if (checkQueue === true) {
      checkQueue = false;
    }
    else {
      checkQueue = true;
      setTimeout(() => {   
        this.deleteActiveQueue(obj.$key);
      }, 90000);
    }
    //console.log(checkQueue)
    return this.db.object('/activeQueses/' + obj.$key).update({ isActive: checkQueue });
  } 
}
