 
import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import 'rxjs/add/operator/map'
import { AuthService } from './auth-service.service';

@Injectable()
export class AuthGuard implements CanActivate{
 
  
  constructor(private auth: AuthService, private router: Router) {

  }
  canActivate(route,state) {    
    return this.auth.user$.map(user => {
      if(user){
        if (user.emailVerified) return true 
      }
       this.router.navigate(['/login'],{queryParams:{returnUrl:state.url}})
      return false;
    })
  }
}
