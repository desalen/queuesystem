import { AppUser } from '../modules/app-user';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database-deprecated';
import * as firebase from 'firebase';

@Injectable()
export class UserService {
    message:string;

    constructor(private db: AngularFireDatabase) {
    }

    //we need to store the user object in firebase
    //for mange the roles of autharzation
    save(user: firebase.User) {
        //console.log("saving the user in DB"+user.displayName);
        // console.log("this is the user",user);
        this.db.list('/admins').push({ 
            isadmin: true,
            email: user.email

        })

    } 

      create(phone) {
        // console.log(dp);
        return this.db.list('/users').push({
            userPhone: phone
        });
    }
    get(uid: String): FirebaseObjectObservable<AppUser> {
        //gets uid and return application user object
        return this.db.object('/users/' + uid);
    }
    async getOrcreateCart(phone): Promise<string> { 
        if (phone) return this.message;
    
        let result = await this.create(phone);  
        return result.key;
      }
}
