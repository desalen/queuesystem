
import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';

@Injectable()
export class QueueService {

  constructor(private db: AngularFireDatabase) { }

 
  getAll(): FirebaseListObservable<any[]> {
    return this.db.list('/queues/');
  }
  create(queue) { 
    // {dpick: Wed Nov 22 2017 00:00:00 GMT+0200 (Eastern Europe Standard Time), barber: "Gid"}
    let dp=queue.dpick.toLocaleString();
    let barber=queue.barber;
   // console.log(dp);
    return this.db.list('/queues').push({
      dateCreated: new Date().getTime(),
      queueTime:dp,
      barberName:barber
    });
  }
  delete(key){
    // console.log(key);
    return this.db.object('/queues/'+key)
    .remove()
    .then(x=>console.log("the column as been deleted"));
  }
  expiredQueues(){

  }
}
