import { WindowService } from './window.service';
import { UserService } from './user.service';
import { AppUser } from './../modules/app-user';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthService {  
  authState: any = null;

  user$: Observable<firebase.User>;
  constructor(
    private userService: UserService,
    private afAuth: AngularFireAuth,
    private router: Router,
    private route: ActivatedRoute) {
    this.user$ = afAuth.authState; 
  } 
  login(formData) {  
    // console.log(formData)
    let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    localStorage.setItem('returnUrl', returnUrl);
    // this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider()); 
    firebase.auth().signInWithEmailAndPassword(formData.Email, formData.password)
        .then((user) => {
          this.authState = user 
          this.router.navigate(['/queue'])  
          this.getCurrentUserToken(); 
        })
      .catch(function (error) {  
        var errorCode = error.code;
        var errorMessage = error.message;  
      console.log(errorCode);
      });

  }

  logout() {
    this.afAuth.auth.signOut();
    localStorage.removeItem('isLoggedIn');
  }
  get appUser$(): Observable<AppUser> {
    return this.user$
      .switchMap(user => { 
        if (user) return this.userService.get(user.uid);
         return Observable.of(null);
      });
  }
  getCurrentUserToken(){
    firebase.auth().currentUser.getToken()
    .then(
      (token: string) => {
        localStorage.setItem('isLoggedIn', token);
      }
    )
    localStorage.getItem('isLoggedIn');
    }
   loginWithGoogle() {
      return this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider()); 
   }
}
