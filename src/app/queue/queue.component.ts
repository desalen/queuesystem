import { Subscription } from 'rxjs/Rx';
import { Queue } from '../modules/queue';
import { QueueService } from './../services/queue.service';
import { Component, OnInit } from '@angular/core'; 

@Component({
  selector: 'queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.less']
})
export class QueueComponent {
  queueObj = {};
  subscription: Subscription;
  queues: Queue[] = [];
  name: string;
  private _opened: boolean = false;
  private min = new Date(new Date().setDate(new Date().getDate() - 1));
  private max = new Date(new Date().setDate(new Date().getDate() + 4)); 

  private _toggleSidebar() {
    this._opened = !this._opened;
  }

  constructor(private queueService: QueueService ) {
    this.subscription = this.queueService.getAll()
      .subscribe(queues => this.queues = queues);
  }

  save(data) { 
    // console.log(data) 
      this.queueObj = data; 
      this.queueService.create(data).then(() => {
        console.log('saved succfully')    
      }, error => {
        console.error('error ' + error.message);
      });; 
  }
  deleteQueue(key) {
    if (confirm("האם אתה בטוח?")) {
      this.queueService.delete(key);
    }
  }
}
