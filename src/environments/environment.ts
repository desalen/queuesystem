// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  firebase:{
    apiKey: "AIzaSyBo_CE108_obs8Y3nBFQbLT-4vydJb65so",
    authDomain: "queuesystem-3063b.firebaseapp.com",
    databaseURL: "https://queuesystem-3063b.firebaseio.com",
    projectId: "queuesystem-3063b",
    storageBucket: "queuesystem-3063b.appspot.com",
    messagingSenderId: "745059779027"
  }
};
